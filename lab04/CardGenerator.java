/////////
//// CSE 002 Card Generator
//// Ryan Matthiessen
//// Last Edited 9/20/18
//// Pick up a random card from the deck using a number generator (1-52, inclusive)

public class CardGenerator {
  
  public static void main (String[] args) {
    
    // Create a random number between 1 and 52, inclusive;
    int pickedCard = (int)(Math.random() * 52 + 1);
    
    // Declare and initialize variables used to determine number and suit
    int suit = pickedCard; 
    int cardNumber = pickedCard % 13;
    String cardIdentity = "";
    
    System.out.print("Your card is the ");
    
    // Switch statement used to determine card number (cardIdentity)
    switch (cardNumber){
      case 0:
        cardIdentity = ("ace of ");
        break;
      case 1:
        cardIdentity = ("2 of ");
        break;
      case 2:
        cardIdentity = ("3 of ");
        break;
      case 3:
        cardIdentity = ("4 of ");
        break;
      case 4:
        cardIdentity = ("5 of ");
        break;
      case 5:
        cardIdentity = ("6 of ");
        break;
      case 6:
        cardIdentity = ("7 of ");
        break;
      case 7:
        cardIdentity = ("8 of ");
        break;
      case 8:
        cardIdentity = ("9 of ");
        break;
      case 9:
        cardIdentity = ("10 of ");
        break;
      case 10:
        cardIdentity = ("jack of ");
        break;
      case 11:
        cardIdentity = ("queen of ");
        break;
      case 12:
        cardIdentity = ("king of ");
        break;
    }
    
    // Prints out the number of the card
    System.out.print(cardIdentity);
    
    // If statement used to determine suit of card (suit)
    if (suit >= 1 && suit <= 13){
      System.out.println("diamonds");
    }
    else if (suit >= 14 && suit <= 26){
      System.out.println("clubs");
    }
    else if (suit >= 27 && suit <= 39){
      System.out.println("hearts");
    }
    else if (suit >= 40 && suit <= 52){
      System.out.println("spades");
    }
  }
}