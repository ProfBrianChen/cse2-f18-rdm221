/////////
//// CSE 002 Poker
//// Ryan Matthiessen
//// Last Edited 10/9/2018
//// Receive input for amount of hands. Loop that many times, calculating cards, holding probabilities, and not allowing repeat cards

// Import of scanner to get user input
import java.util.Scanner;

public class Hw05 {
  
  public static void main (String args[]) {
    
    // Declare and initialize variables used to determine number and match cards
    int card1, c1Num, card1Match = 0;
    int card2, c2Num, card2Match = 0;
    int card3, c3Num, card3Match = 0;
    int card4, c4Num, card4Match = 0;
    int card5, c5Num = 0;
    int onePair = 0;
    int twoPair = 0;
    int threeOfAKind = 0;
    int fourOfAKind = 0;
    int currentHand = 1;
    int totalHand = 0;
    Scanner myScanner = new Scanner(System.in);
    
    // Ask the user how many hands to generate, if not provided with int, ask until user provides int
    System.out.print("How many hands would you like to generate: ");
    while (!myScanner.hasNextInt()) {
      System.out.print("The type was invalid, please re-enter your information in the type integer: ");
      myScanner.next();
    }
   // Total hands is used for loop and probability
    totalHand = myScanner.nextInt();
    
    // While loop that runs the total number of hands entered by user
    while (currentHand <= totalHand) {
    // Picks 5 cards from the deck. If a repeat card is pulled, it will be discarded and a new card pulled
      card1 = (int)(Math.random() * 52 + 1);
      card2 = (int)(Math.random() * 52 + 1);
      while (card2 == card1) {
        card2 = (int)(Math.random() * 52 + 1);
      }
      card3 = (int)(Math.random() * 52 + 1);
      while (card3 == card2 || card3 == card1) {
        card3 = (int)(Math.random() * 52 + 1);
      }
      card4 = (int)(Math.random() * 52 + 1);
      while (card4 == card3 || card4 == card2 || card4 == card1) {
        card4 = (int)(Math.random() * 52 + 1);
      }
      card5 = (int)(Math.random() * 52 + 1);
      while (card5 == card4 || card5 == card3 || card5 == card2 || card5 == card1) {
        card5 = (int)(Math.random() * 52 + 1);
      }
    // Calculates the face value of the card (ace = 1 ... king = 13)
      c1Num = card1 % 13;
      c2Num = card2 % 13;
      c3Num = card3 % 13;
      c4Num = card4 % 13;
      c5Num = card5 % 13;
    // If statement to force multiples of 13 to king
      if (card1 % 13 == 0) {
        c1Num = 13;
      }
      if (card2 % 13 == 0) {
        c2Num = 13;
      }
      if (card3 % 13 == 0) {
        c3Num = 13;
      }
      if (card4 % 13 == 0) {
        c4Num = 13;
      }
      if (card5 % 13 == 0) {
        c5Num = 13;
      }
    // Calculates if a match is made from the five cards. If a match, it is tally'd
      if (c1Num == c2Num) {
          card1Match++;
      }
      if (c1Num == c3Num) {
        card1Match++;
      }
      if (c1Num == c4Num) {
        card1Match++;
      }
      if (c1Num == c5Num) {
        card1Match++;
      }
      if (c2Num == c3Num) {
        card2Match++;
      }
      if (c2Num == c4Num) {
        card2Match++;
      }
      if (c2Num == c5Num) {
        card2Match++;
      }
      if (c3Num == c4Num) {
        card3Match++;
      }
      if (c3Num == c5Num) {
        card3Match++;
      }
      if (c4Num == c5Num) {
        card4Match++;
      }
    // Switch statement to calculate the one-pair and the special cases (two-pair, three-of-akind, four-of-a-kind) for all cards
      switch (card1Match) {
        case 1:
          if (card1Match == card2Match || card1Match == card3Match || card1Match == card4Match) {
          twoPair++;
          } else {
            onePair++;
          }
          break;
        case 2:
          threeOfAKind++;
          break;
        case 3:
          fourOfAKind++;
          break;
      }
      
      switch (card2Match) {
        case 1:
          if (card2Match == card3Match || card2Match == card4Match) {
          twoPair++;
          } else {
            onePair++;
          }
          break;
        case 2:
          twoPair++;
          break;
        case 3:
          threeOfAKind++;
          break;
      }
      switch (card3Match) {
        case 1:
          if (card3Match == card4Match) {
          twoPair++;
          } else {
            onePair++;
          }
          break;
        case 2:
          twoPair++;
          break;
      }
      switch (card4Match) {
        case 1:
          onePair++;
          break;
      }
    // Resets the card matches for the next iteration of the loop. Increments currentHand for the next iteration
      card1Match = 0;
      card2Match = 0;
      card3Match = 0;
      card4Match = 0;
      currentHand++;
    }
    // calculates the probabilities and prints them, along with the total number of hands by user input
    double oneProb = onePair / (double) totalHand;
    double twoProb = twoPair / (double) totalHand;
    double threeProb = threeOfAKind/ (double) totalHand;
    double fourProb = fourOfAKind / (double) totalHand;
    System.out.println("The total number of hands: " + totalHand);
    System.out.printf("The probability of One-pair: %4.3f\n", oneProb);
    System.out.printf("The probability of Two-pair: %4.3f\n", twoProb);
    System.out.printf("The probability of Three-of-a-kind: %4.3f\n", threeProb);
    System.out.printf("The probability of Four-of-a-kind: %4.3f\n", fourProb);
  }
}