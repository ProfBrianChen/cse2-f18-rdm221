/////////
//// CSE 002 hw07
//// Ryan Matthiessen
//// Last Edited 10/28/2018
//// Ask the user to input a string, give them the options to manipulate their string a certain number of ways

  // Imports scanner class to allow for user input
import java.util.Scanner;

public class hw07 {
  // Declares the scanner for use throughout the class
  static Scanner stringScanner = new Scanner(System.in);
  
  // SampleText method asks for the user to input their string, saves it, and reiterates it
  public static String sampleText () {
    
    System.out.print("Please enter a sample text: ");
    String text = stringScanner.nextLine();
    System.out.println("You entered: " + text);
    return text;
    
  }
  
  // PrintMenu method gives the user the option to manipulate their string several different ways multiple times
  public static void printMenu(String input) {
    
  // Declares and initializes variable 'answer' which is the input for the number of manipulations
    char answer = ' ';
    
  // Do-while loop to print the menu and ask for input infinitely as long as 'q' (exit) is not pressed
    do {
      String pickWord = "";
      System.out.println("MENU ");
      System.out.println("c - Number of non-whitespace characters");
      System.out.println("w - Number of words");
      System.out.println("f - Find text");
      System.out.println("r - Replace all !");
      System.out.println("s - Shorten spaces");
      System.out.println("q - Quit");
      System.out.print("Choose an option: ");
      
   // Allows the user to input a letter of their choosing
      answer = stringScanner.nextLine().charAt(0);
      
   // If statement to execute the different methods or exit if 'q' is pressed. On invalid input, prompts the user to re-enter
      if (answer == 'c') {
        getNumOfNonWSCharacters(input);
      } else if (answer == 'w') {
        getNumberOfWords(input);
      } else if (answer == 'f') {
        System.out.print("Enter a word or phrase to be found: ");
        pickWord = stringScanner.nextLine();
        findText(pickWord, input);
      } else if (answer == 'r') {
        String replace = replaceExclamation(input);
        System.out.println("Edited text: " + replace);
      } else if (answer == 's') {
        String shorten = shortenSpace(input);
        System.out.println("Edited text: " + shorten);
      } else if (answer == 'q') {
        System.exit(0);
      } else {
        System.out.print("Invalid input, please re-enter: ");
      }
    } while (answer != 'q');
  }
  
   // getNumOfNonWSCharacters method states the amount of non-whitespace characters (such as 'a' or 'b'). Returns the amount of characters
  public static int getNumOfNonWSCharacters (String text) {
    int length = text.length();
    int counter = text.length();
    
   // For loop checks every char for the length of the string inputted. For every space character, decreases the counter by 1 (counter starts at string length)    
    for (int i = 0; i < length; i++) {
      if (text.charAt(i) == ' ') {
        counter--;
      }
    }
    System.out.println("Number of non-whitespace characters: " + counter);
    return counter;
  }
  
   // getNumOfWords method states the amount of words (such as "and" or "the"). Returns the amount of words    
  public static int getNumberOfWords (String text) {
   // Removes any extra spaces which may be inputted by user 
    text = text.replaceAll("\\s+", " ");
    int length = text.length();
    int wordCount = 0;
    
   // For loop checks for every space for the length of the string inputted. For every space character, increases the counter by 1
    for (int i = 0; i < length; i++) {
      if (text.charAt(i) == ' ') {
        wordCount++;
      }
    }
    wordCount++;
    System.out.println("Number of words: " + wordCount);
    return wordCount;
  }
   // findText method searches for the inputted word inside the inputted string. For every instance, increases the count by 1. Returns the amount of matches   
  public static int findText (String pickWord, String text) {
    
    int matchCount = 0;
    int length = text.length();
    int i = 0;
    
   // While loop looks for the starting index of the next word match. If match occurs, matchCount increments. If no match is found, loop ends
    while (i < length) {
      if (text.indexOf(pickWord, i) != -1) {
        matchCount++;
        i = text.indexOf(pickWord, i);
        i++;
      } else {
        i = length + 1;
      }
    }
    System.out.println("\'" + pickWord + "\' instances: " + matchCount);
    return matchCount;
  }
  
   // replaceExclamation method replaces all the exclamation marks with periods. Returns the edited text    
  public static String replaceExclamation (String text) {
   // Declares and initializes the related variables
    char exclamation = '!';
    char period = '.';
    
   // .replace is used to scan the string for exclamation marks and replace them with the periods. Overwrites the old text
    text = text.replace(exclamation , period);
    return text;
  }
  
   // shortenSpace method replaces all the extra spaces with only one space. Returns the edited text
  public static String shortenSpace (String text) {
    
   // .replaceAll is used to scan the string for different functions (one of them is extra spaces) and replaces them with a single space. Overwrites the old text
    text = text.replaceAll("\\s+", " ");
    return text;
  }
  
   // Main method to save the inputted text from the sampleText method and calls the printMenu method for manipulations using the inputted text
  public static void main (String args[]) {
    
    String text = sampleText();
    printMenu(text);
  }
}