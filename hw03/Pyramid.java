//////////
//// CSE 002 Pyramid
//// Ryan Matthiessen
//// Last Edited: 9/14/2018
//// Ask the user for the dimensions of a pyramid and returns the volume inside the pyramid.

// Imports the scanner class which will be used to receive user input.
import java.util.Scanner;

public class Pyramid{
  
  public static void main (String[] args){
   
   // Declares a scanner named "myScanner".
   Scanner myScanner = new Scanner(System.in);
    
    // Asks for user information regarding the dimensions of the pyramid.
    System.out.print("One side of the square pyramid base (length) is: ");
    double length = myScanner.nextDouble();
    System.out.print("The height of the pyramid is: ");
    double height = myScanner.nextDouble();
    
    // Declares and initializes the variables used to calculate for volume inside the pyramid.
    double baseArea = Math.pow(length,2);
    double volume = (baseArea * height) / 3;
    
    // Prints the volume inside the pyramid.
    System.out.println("The volume of the pyramid is: " + volume + " cubic units.");
  }
}