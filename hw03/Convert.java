//////////
//// CSE 002 Convert
//// Ryan Matthiessen
//// Last Edited: 9/14/2018
//// Ask the user for land acres and rainfall amount from a hurricane. Convert rain amount into cubic miles from inches.

// Imports the scanner class which will be used to receive user input.
import java.util.Scanner;

public class Convert{
  
  public static void main (String[] args){
    
    // Declares a new scanner named "myScanner".
    Scanner myScanner = new Scanner(System.in);
    
    // Asks the user for the land acres affected and rainfall amount. Converts that information into a double with scanner.
    System.out.print("Enter the affected rainfall in acres: ");
    double acres = myScanner.nextDouble();
    System.out.print("Enter the rainfall of the affected area in inches: ");
    double inches = myScanner.nextDouble();
    
    // Declares and initializes the variables related to the correct conversions.
    double feet = inches / 12;
    double miles = feet / 5280;
    double feetInAcres = Math.sqrt(acres * 43560);
    double milesinAcres = feetInAcres / 5280;
    double squareMilesInAcres = Math.pow(milesinAcres,2);
    double cubicMiles = miles * squareMilesInAcres;
    
    // Prints the amount of rainfall in cubic miles.
    System.out.println("The amount of rain is: " + cubicMiles + " cubic miles.");
  }
}