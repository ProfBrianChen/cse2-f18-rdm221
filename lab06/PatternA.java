/////////
//// CSE 002 PatternA
//// Ryan Matthiessen
//// Last Edited 10/17/2018
//// Ask the user for the rows in a pyramid, increment by one for every row, adding it until the user input is reached

import java.util.Scanner;

public class PatternA {
  
  public static void main (String args[]) {
    
    // Declare and initialize variables used to determine dimensions of number pyramid
    Scanner myScanner = new Scanner(System.in);
    int totalRows = 0;
    int currentRows = 1;
    
    // Ask the user for integer input and checks if the user inputted an integer
    System.out.print("How many rows should the pyramid be (1-10): ");
    while (!myScanner.hasNextInt()) {
      System.out.print("Make sure your input is an integer. Please re-enter: ");
      myScanner.next();
    }
    // Stores the integer given in 'totalRows'
    totalRows = myScanner.nextInt();
    
    // If the integer input is not between 1-10, have the user re-input and store it back in 'totalRows'
    if (totalRows > 10 || totalRows < 1) {
      while (totalRows > 10 || totalRows < 1) {
        System.out.print("Make sure your integer is no less than 1 and no greater than 10. Please re-enter:");
        totalRows = myScanner.nextInt();
      }
    }
    
    // For loop determining the height of the number pyramid
    for (int vertical = 1; vertical <= totalRows; vertical++) {
      if (vertical != 1) {
        System.out.println("");
      }
      
    // For loop incrementing the numbers as going from left to right
      for (int horizontal = 1; horizontal <= vertical; horizontal++) {
        System.out.print(horizontal + " ");
      }
    }
    System.out.println("");
  }
}