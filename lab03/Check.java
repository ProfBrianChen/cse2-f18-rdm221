//////////////
//// CSE 002 Check
//// Ryan Matthiessen
//// Last Updated: 9/13/2018
//// Ask for user input data and use that data to perform calculations regarding payment information for a check

import java.util.Scanner;

public class Check{
  
  public static void main(String[] args){
    
    // Adds a new scanner called myScanner
    Scanner myScanner = new Scanner(System.in);
    
    // Asks user input for the check cost, tip percentage, and number of people. Turns tip percentage into a decimal.
    System.out.print("Enter the original cost of the check in form xx.xx ");
    double checkCost = myScanner.nextDouble();
    System.out.print("Enter the percentage tip that you wish to pay as a whole number in form xx: ");
    double tipPercent = myScanner.nextDouble();
    tipPercent /= 100;
    System.out.print("Enter the number of people that went out to dinner: ");
    int numPeople = myScanner.nextInt();
    
    // Declares and assigns variables related to the cost of payment per person
    double totalCost;
    double costPerPerson;
    int dollars;
    int dimes;
    int pennies;
    totalCost = checkCost * (1 + tipPercent);
    costPerPerson = totalCost / numPeople;
    dollars = (int)costPerPerson;
    dimes = (int)(costPerPerson * 10) % 10;
    pennies = (int)(costPerPerson * 100) % 10;
    
    // States the cost per person in dollar and change ($x.xx) format
    System.out.println("Each person must pay: $" + dollars + "." + dimes + pennies);
  }
  
}