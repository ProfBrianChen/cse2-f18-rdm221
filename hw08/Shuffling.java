/////////
//// CSE 002 Shuffling
//// Ryan Matthiessen
//// Last Edited 11/13/2018
//// Given a deck of cards, shuffle the deck and a hand of cards (5) using the cards at the bottom of the shuffled deck (last indicies)

  // Import scanner for getting user input
import java.util.Scanner;

public class Shuffling {
  
  // Method 'shuffle' used to swap card of first index with a card of a random index (shuffles the deck)
  public static void shuffle (String[] cards) {
    String temp = "";
    int counter = 0;
  // While loop for a total of 100 card swaps
    while (counter <= 100) {
      int random = (int)(Math.random() * 51 + 1);
  // Stores one swapped card in 'temp' variable. Overrides index 0 card with swapped variable, overrides random index card with 'temp' variable
      temp = cards[0];
      cards[0] = cards[random];
      cards[random] = temp;
      counter++;
    }
    System.out.println("Shuffled");
  }
  
  // Method 'getHand' pulls the card values of the last indices.
  public static String[] getHand (String[] cards, int index, int numCards) {
  // Declaration and initialization of new string array 'Hand'
    String[] hand = new String[numCards];
  // For loop to determine the pulled hand. For everytime the user enters a '1' in main, 5 more cards from the bottom of the deck are pulled out
    for (int i = 0; i < numCards; i++) {
      hand[i] = cards[index];
      index--;
    }
    System.out.println("Hand");
    return hand;  
  }
  
  // Method 'printArray' prints the card values which will print both the total deck of cards and the pulled hand
  public static void printArray (String[] cards) {
  
  //For loop to print out the value of the string in each index of the array 'cards'
    for (int i = 0; i < cards.length; i++) {
      System.out.print(cards[i] + " ");
    }
    System.out.println();
  }
  
  public static void main (String[] args) { 
    
    Scanner scan = new Scanner(System.in); 
    
    //suits club, heart, spade or diamond 
    String[] suitNames={"C","H","S","D"};    
    String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; 
    String[] cards = new String[52]; 
    String[] hand = new String[5]; 
    int numCards = 5; 
    int again = 1; 
    int index = 51;
    for (int i = 0; i < 52; i++) { 
      cards[i] = rankNames[i % 13] + suitNames[i / 13];
    } 
    System.out.println();
    printArray(cards); 
    shuffle(cards); 
    printArray(cards); 
    
    // Asks user to pull a new hand by entering '1' if they want
    while(again == 1) { 
    // If statement to draw a new deck and shuffle it if under full hand requirement
      if (index < 4) {
        shuffle(cards);
        printArray(cards);
        index = 51;
      }
      hand = getHand(cards, index, numCards); 
      printArray(hand);
      index = index - numCards;
      System.out.println("Enter a 1 if you want another hand drawn"); 
      again = scan.nextInt(); 
    }  
  } 
}
