/////////
//// CSE 002 Code
//// Ryan Matthiessen
//// Last Edited 10/9/2018
//// Ask user for a bunch of information related to courses, reiterate that information back

// Imports scanner for user input
import java.util.Scanner;

public class Lab05 {
  
  public static void main (String args[]) {
    
    // Decalration and initialization of variables
    int courseNumber = 0;
    String departmentName = "";
    int timesMet = 0;
    String classStartTime = "";
    String instructorName = "";
    int numberOfStudents = 0;
    Scanner intScanner = new Scanner(System.in);
    Scanner stringScanner = new Scanner(System.in);
    
    // Ask for the course number and stores it in variable 'courseNumber'
    System.out.print("Please enter the course number of your class: ");
    while (!intScanner.hasNextInt()) {
      System.out.print("The type was invalid, please re-enter your information in the type integer: ");
      intScanner.next();
    }
    courseNumber = intScanner.nextInt();
      
    // Ask for the department name and stores it in variable 'departmentName'
    System.out.print("Please enter the department name in which your course resides: ");
    while (!stringScanner.hasNext()) {
      System.out.print("The type was invalid, please re-enter your information in the type string: ");
      stringScanner.next();
    }
    departmentName = stringScanner.nextLine();
    
    // Ask for the number of times the course meets in a week and stores it in variable 'timesMet'
    System.out.print("Please enter the amount of times your course meets: ");
    while (!intScanner.hasNextInt()) {
      System.out.print("The type was invalid, please re-enter your information in the type integer: ");
      intScanner.next();
    }
    timesMet = intScanner.nextInt();
    
    // Ask for the time the class starts (twice using hours and minutes) and stores it in variable 'classStartTime'
    System.out.print("Please enter your class starting time (hours): ");
    while (!intScanner.hasNextInt()) {
      System.out.print("The type was invalid, please re-enter your information in the type integer: ");
      intScanner.next();
    }
    classStartTime = intScanner.next();
    intScanner.nextLine();
    System.out.print("Please enter your class starting time (minutes): ");
    while (!intScanner.hasNextInt()) {
      System.out.print("The type was invalid, please re-enter your information in the type integer: ");
      intScanner.next();
    }
    // Adds the minute time to the previously stored hour time
    classStartTime = classStartTime + ":" + intScanner.nextInt();
    
    // Ask for the instructor name and stores it in variable 'instructorName'
    System.out.println("Please enter the name of the instructor:");
    while (!stringScanner.hasNext()) {
      System.out.print("The type was invalid, please re-enter your information in the type string: ");
      stringScanner.next();
    }
    instructorName = stringScanner.nextLine();
    
    // Ask for the number of students and stores it in variable 'numberOfStudents'
    System.out.println("Please enter the number of students in the course:");
    while (!intScanner.hasNextInt()) {
      System.out.print("The type was invalid, please re-enter your information in the type integer: ");
      intScanner.next();
    }
    numberOfStudents = intScanner.nextInt();
    
    // Prints all the information inputted by the user
    System.out.printf("Course Number: %d\n",courseNumber);
    System.out.printf("Department Name: %s\n",departmentName);
    System.out.printf("Times Course Meets: %d\n",timesMet);
    System.out.printf("Start Time of Class: %s\n",classStartTime);
    System.out.printf("Instructor Name: %s\n",instructorName);
    System.out.printf("Number of students in class: %d\n",numberOfStudents);
    
  }
}