//////////
//// CSE 002 CrapsIf
//// Ryan Matthiessen
//// Last Edited: 9/23/2018
//// Ask the user to roll or choose two dice numbers, calculate and print their score as well as their respective terminologies.

import java.util.Scanner;

public class CrapsIf{
  
  public static void main (String[] args){
    
    // Declares and initializes the two dice variables and total score
    int dice1 = 0;
    int dice2 = 0;
    int score = 0;
    
    // Creates the variable 'myScanner' and asks the user for input while retreiving it in 'diceChoice'
    Scanner myScanner = new Scanner(System.in);
    System.out.print("Input '1' to randomly cast dice. Input '2' to state dice for evaluation: ");
    int diceChoice = myScanner.nextInt();
    
    // If statement to denote the two inputs, either randomly generate the dice numbers or allow the user to choose them
    if (diceChoice == 1) {
      dice1 = (int)(Math.random() * 6 + 1);
      dice2 = (int)(Math.random() * 6 + 1);
    } else if (diceChoice == 2) {
      System.out.print("Input the number of the first die from 1 to 6: ");
      dice1 = myScanner.nextInt();
      System.out.print("Input the number of the second die from 1 to 6: ");
      dice2 = myScanner.nextInt();
      if (dice1 > 6 || dice1 < 1 || dice2 > 6 || dice2 < 0) {
        System.out.println("Atleast one die had an incorrect input, closing the application.");
        System.exit(0);
    }
    }
    
    // Prints the score of the two dice
    score = dice1 + dice2;
    System.out.println("You rolled a " + dice1 + " and a " + dice2 + " for a total score of " + score + ".");
    
    // If statement to denote the terminology in rolling the two dice. Prints that terminology.
    if (dice1 == 1 && dice2 == 1) {
      System.out.println("You rolled a Snake Eyes.");
    } else if ((dice1 == 1 && dice2 == 2) || (dice1 == 2 && dice2 == 1)) {
      System.out.println("You rolled an Ace Deuce.");
    } else if ((dice1 == 1 && dice2 == 3) || (dice1 == 3 && dice2 == 1)) {
      System.out.println("You rolled an Easy Four.");
    } else if ((dice1 == 1 && dice2 == 4) || (dice1 == 4 && dice2 == 1)) {
      System.out.println("You rolled a Fever Five.");
    } else if ((dice1 == 1 && dice2 == 5) || (dice1 == 5 && dice2 == 1)) {
      System.out.println("You rolled an Easy Six.");
    } else if ((dice1 == 1 && dice2 == 6) || (dice1 == 6 && dice2 == 1)) {
      System.out.println("You rolled a Seven Out.");
    } else if (dice1 == 2 && dice2 == 2) {
      System.out.println("You rolled a Hard Four.");
    } else if ((dice1 == 2 && dice2 == 3) || (dice1 == 3 && dice2 == 2)) {
      System.out.println("You rolled a Fever Five.");
    } else if ((dice1 == 2 && dice2 == 4) || (dice1 == 4 && dice2 == 2)) {
      System.out.println("You rolled an Easy Six.");
    } else if ((dice1 == 2 && dice2 == 5) || (dice1 == 5 && dice2 == 2)) {
      System.out.println("You rolled a Seven Out.");
    } else if ((dice1 == 2 && dice2 == 6) || (dice1 == 6 && dice2 == 2)) {
      System.out.println("You rolled an Easy Eight.");
    } else if (dice1 == 3 && dice2 == 3) {
      System.out.println("You rolled a Hard Six.");
    } else if ((dice1 == 3 && dice2 == 4) || (dice1 == 4 && dice2 == 3)) {
      System.out.println("You rolled a Seven Out.");
    } else if ((dice1 == 3 && dice2 == 5) || (dice1 == 5 && dice2 == 3)) {
      System.out.println("You rolled an Easy Eight.");
    } else if ((dice1 == 3 && dice2 == 6) || (dice1 == 6 && dice2 == 3)) {
      System.out.println("You rolled a Nine.");
    } else if (dice1 == 4 && dice2 == 4) {
      System.out.println("You rolled an Hard Eight.");
    } else if ((dice1 == 4 && dice2 == 5) || (dice1 == 5 && dice2 == 4)) {
      System.out.println("You rolled a Nine.");
    } else if ((dice1 == 4 && dice2 == 6) || (dice1 == 6 && dice2 == 4)) {
      System.out.println("You rolled a Easy Ten.");
    } else if (dice1 == 5 && dice2 == 5) {
      System.out.println("You rolled a Hard Ten.");
    } else if ((dice1 == 5 && dice2 == 6) || (dice1 == 6 && dice2 == 5)) {
      System.out.println("You rolled a Yo-leven.");
    } else if (dice1 == 6 && dice2 == 6) {
      System.out.println("You rolled a Boxcars.");
    }
  }
}