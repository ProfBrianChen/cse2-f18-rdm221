/////////
//// CSE 002 CrapsSwitch
//// Ryan Matthiessen
//// Last Edited: 9/23/2018

import java.util.Scanner;

public class CrapsSwitch{
  
  public static void main (String[] args) {
    
    // Declares and initializes the two dice variables and total score
    int dice1 = 0;
    int dice2 = 0;
    int score = 0;

    // Creates the variable 'myScanner' and asks the user for input while retreiving it in 'diceChoice'
    Scanner myScanner = new Scanner(System.in);
    System.out.print("Input '1' to randomly cast dice. Input '2' to state dice for evaluation: ");
    int diceChoice = myScanner.nextInt();
    
    
    // Switch statement to denote the two inputs, either randomly generate the dice numbers or allow the user to choose them
    switch (diceChoice) {
      // Random generation of dice
      case 1:
        dice1 = (int)(Math.random() * 6 + 1);
        dice2 = (int)(Math.random() * 6 + 1);
        score = dice1 + dice2;
        System.out.println("You rolled a " + dice1 + " and a " + dice2 + " for a total score of " + score + ".");
        break;
      // Choose the dice numbers
      case 2:
        System.out.print("Input the number of the first die from 1 to 6: ");
        dice1 = myScanner.nextInt();
        System.out.print("Input the number of the second die from 1 to 6: ");
        dice2 = myScanner.nextInt();
        // Check that both dice numbers are between 1-6, inclusive
        switch (dice1) {
          case 1:
          case 2:
          case 3:
          case 4:
          case 5:
          case 6:
            score = dice1 + dice2;
            break;
          default:
            System.out.println("Atleast one die had an incorrect input, closing the application.");
            System.exit(0);
            break;
        }
          switch (dice2) {
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
              score = dice1 + dice2;
              System.out.println("You rolled a " + dice1 + " and a " + dice2 + " for a total score of " + score + ".");
              break;
            default:
              System.out.println("Atleast one die had an incorrect input, closing the application.");
              System.exit(0);
              break;
          }
        break;
      default:
        System.out.println("Your input was incorrect and not accepted, closing the application.");
        System.exit(0);
        break; 
    }
    
    // Switch statement to denote the terminology in rolling the two dice. Prints that terminology for the chosen scenario.
    switch (dice1) {
      case 1:
        switch (dice2) {
          case 1:
            System.out.println("You rolled a Snake Eyes.");
            break;
          case 2:
            System.out.println("You rolled an Ace Deuce.");
            break;
          case 3:
            System.out.println("You rolled an Easy Four.");
            break;
          case 4:
            System.out.println("You rolled a Fever Five.");
            break;
          case 5:
            System.out.println("You rolled an Easy Six.");
            break;
          case 6:
            System.out.println("You rolled a Seven Out.");
            break;
        }
      break;
      case 2:
        switch (dice2) {
          case 1:
            System.out.println("You rolled an Ace Deuce.");
            break;
          case 2:
            System.out.println("You rolled a Hard Four.");
            break;
          case 3:
            System.out.println("You rolled a Fever Five.");
            break;
          case 4:
            System.out.println("You rolled an Easy Six.");
            break;
          case 5:
            System.out.println("You rolled a Seven Out.");
            break;
          case 6:
            System.out.println("You rolled an Easy Eight.");
            break;
        }
      break;
      case 3:
        switch (dice2) {
          case 1:
            System.out.println("You rolled an Easy Four.");
            break;
          case 2:
            System.out.println("You rolled a Fever Five.");
            break;
          case 3:
            System.out.println("You rolled a Hard Six.");
            break;
          case 4:
            System.out.println("You rolled a Seven Out.");
            break;
          case 5:
            System.out.println("You rolled an Easy Eight.");
            break;
          case 6:
            System.out.println("You rolled a Nine.");
            break;
        }
      break;
      case 4:
        switch (dice2) {
          case 1:
            System.out.println("You rolled a Fever Five.");
            break;
          case 2:
            System.out.println("You rolled an Easy Six.");
            break;
          case 3:
            System.out.println("You rolled a Seven Out.");
            break;
          case 4:
            System.out.println("You rolled a Hard Eight.");
            break;
          case 5:
            System.out.println("You rolled a Nine.");
            break;
          case 6:
            System.out.println("You rolled an Easy Ten.");
            break;
        }
      break;
      case 5:
        switch (dice2) {
          case 1:
            System.out.println("You rolled an Easy Six.");
            break;
          case 2:
            System.out.println("You rolled a Seven Out.");
            break;
          case 3:
            System.out.println("You rolled an Easy Eight.");
            break;
          case 4:
            System.out.println("You rolled a Nine.");
            break;
          case 5:
            System.out.println("You rolled a Hard Ten.");
            break;
          case 6:
            System.out.println("You rolled a Yo-leven.");
            break;
        }
      break;
      case 6:
        switch (dice2) {
          case 1:
            System.out.println("You rolled a Seven Out.");
            break;
          case 2:
            System.out.println("You rolled an Easy Eight.");
            break;
          case 3:
            System.out.println("You rolled a Nine.");
            break;
          case 4:
            System.out.println("You rolled an Easy Ten.");
            break;
          case 5:
            System.out.println("You rolled a Yo-leven.");
            break;
          case 6:
            System.out.println("You rolled a Boxcars.");
            break;
        }
      break;
   }
  }
}