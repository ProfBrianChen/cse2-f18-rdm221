//////////////////
//// CSE 002 Welcome Class
//// Ryan Matthiessen
public class WelcomeClass{
  
  public static void main(String args[]){
    ///Welcomes the user and displays my LIN
    System.out.println("    -----------    ");
    System.out.println("    | WELCOME |    ");
    System.out.println("    -----------    ");
    System.out.println("   ^  ^  ^  ^  ^  ^   ");
    System.out.println("  / \\/ \\/ \\/ \\/ \\/ \\");
    System.out.println(" <-R--D--M--2--2--1-> ");
    System.out.println("  \\ /\\ /\\ /\\ /\\ /\\ /");
    System.out.println("   v  v  v  v  v  v   ");
                       
  }
    
}