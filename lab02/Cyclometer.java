/////////////
//// CSE 002 Cyclometer
//// Ryan Matthiessen
//// Last edited: 9/6/2018
//// Print the minutes, rotation count, distance traveled in miles for trips 1, 2, and both trips combined
public class Cyclometer{
  
  public static void main (String args[]){
    
    // Variable declaration and assignment
    int secsTrip1 = 480; // Total amount of time elapsed in seconds for trip 1
    int secsTrip2 = 3220; // Total amount of time elapsed in seconds for trip 2
    int countsTrip1 = 1561; // Total rotation count of wheel for trip 1
    int countsTrip2 = 9037; // Total roation count of wheel for trip 2
    double wheelDiameter = 27.0; // Diameter of the wheel in inches
    double Pi = 3.14159; // Constant value for pi, rounded to 5 decimal places
    int feetPerMile = 5280; // Constant value for number of feet in a mile
    int inchesPerFoot = 12; // Constant value for number of inches in a foot
    double secondsPerMinute = 60; // Constant value for number of seconds in a minute
    double distanceTrip1; // Total distance traveled for trip 1
    double distanceTrip2; // Total distance traveled for trip 2
    double totalDistance; // Total distance traveled for trips 1 and 2 combined
    
    // Prints the amount of time, in minutes, and the rotation count, that both trips took individually
    System.out.println("Trip 1 took " + (secsTrip1/secondsPerMinute) + " minutes and had " + countsTrip1 + " counts.");
	  System.out.println("Trip 2 took " + (secsTrip2/secondsPerMinute) + " minutes and had " + countsTrip2 + " counts.");
    
    // Calculates the distance, in miles, of both trips individually 
    distanceTrip1 = (countsTrip1 * wheelDiameter * Pi) / (inchesPerFoot * feetPerMile);
    distanceTrip2 = (countsTrip2 * wheelDiameter * Pi) / (inchesPerFoot * feetPerMile);
    
    // Calculates the total distance of both miles
    totalDistance = distanceTrip1 + distanceTrip2;
    
    // Prints out the distances from trip1, trip2, and them combined and starts a new line
    System.out.println("Trip 1 was " + distanceTrip1 + " miles");
	  System.out.println("Trip 2 was " + distanceTrip2 + " miles");
	  System.out.println("The total distance was " + totalDistance + " miles");
  }
}