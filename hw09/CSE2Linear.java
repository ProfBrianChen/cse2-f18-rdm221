/////////
//// CSE 002 CSE2Linear
//// Ryan Matthiessen
//// Last Edited 11/26/2018
//// Asks user for 15 inputs, uses binary search to find grade, scrambles array, then uses linear search to find another grade

// Imports Scanner for user input and Random for scrambling the array numbers
import java.util.Random;
import java.util.Scanner;

public class CSE2Linear {
  
// Method 'scramble' used to swap number of first index with a number of a random index
  public static void scramble (int[] array0, int totalInput) {
    int temp = 0;
    int counter = 0;
// While loop for a total of 50 swaps
    while (counter <= 50) {
      int random = (int)(Math.random() * 14 + 1);
// Stores one swapped card in 'temp' variable. Overrides index 0 with swapped variable, overrides random index with 'temp' variable
      temp = array0[0];
      array0[0] = array0[random];
      array0[random] = temp;
      counter++;
    }
// Prints "shuffled" and follows with each number in the scrambled array
    System.out.println("Shuffled");
    for (int i = 0; i < totalInput; i++) {
      System.out.print(array0[i] + " ");
    }
    System.out.println();
  }

// Method 'binary' uses original array 'array0', length of array 'totalInput', and desired grade 'key' to find the grade in the array. Increments the amount of iterations needed
  public static void binary (int[] array0, int totalInput, int key) {
    int high = totalInput;
    int low = 0;
    int mid = 0;
    int counter = 1;
    while (high >= low) {
      mid = (high + low) / 2;
      if (array0[mid] < key) {
        low = mid + 1;
        counter++;
        if (counter == 5 && array0[mid] != key) {
        System.out.println(key + " was not found in the list with " + counter + " iterations");
        break;
        }
      } else if (array0[mid] > key) {
        high = mid - 1;
        counter++;
        if (counter == 5 && array0[mid] != key) {
        System.out.println(key + " was not found in the list with " + counter + " iterations");
        break;
        }
      } else if (array0[mid] == key) {
        System.out.println(key + " was found in the list with " + counter + " iterations");
        break;
      }
    }
  }
// Method 'linear' uses scrambled array 'array0', length of array 'totalInput', and desired grade 'key' to find the grade in the array. Sequentially searches through array0 looking for key  
  public static void linear (int[] array0, int totalInput, int key) {
    
    int match = -1;
    for (int i = 0; i < totalInput; ++i) {
      if (array0[i] == key) {
        match = 1;
        System.out.println(key + " was found in the list with " + (i + 1) + " iterations");
        break;
      }
    }
// If match is not found ('match' stays as -1), state it was not found
    if (match == -1) {
      System.out.println(key + " was not found in the list");
    }
  }

// Method 'main' asks for 15 integers, receives those integers while checking for three possible errors, and calls methods 'binary', 'scramble', and 'linear'
  public static void main (String args[]) {

// Declares and initializes both scanner 'intScanner' and array 'array0'
    Scanner intScanner = new Scanner (System.in);
    int input = 0;
    int totalInput = 15;
    int[] array0 = new int[totalInput];

// Prompts user to enter ints
    System.out.print("Enter 15 ascending ints for final grades in CSE2: " );
    for (int i = 0; i < totalInput; i++) {
      
// Checks to make sure input is of type int
      while (!intScanner.hasNextInt()) {
        System.out.print("Make sure your input is an integer: ");
        intScanner.next();
      }
      input = intScanner.nextInt();
// Checks to make sure input is between 0-100 (and is an int)
      while (input > 100 || input < 0) {
        System.out.print("Make sure your input is an integer is between 0-100: ");
        if (intScanner.hasNextInt()) {
          input = intScanner.nextInt();
        }
      }
// Checks to make sure input is greater than previous input (and is an int)
      while (i > 0 && input < array0[i-1]) {
        System.out.print("Make sure this input is greater than your previous input:");
        if (intScanner.hasNextInt()) {
          input = intScanner.nextInt();
        }
      }
// Sets 'array0' at index 'i' to the inputted value 'input'
      array0[i] = input;
      if (i != 14) {
        System.out.print("Enter int #" + (i+2) + ": ");
      }
    }
// Repeats the values that the user inputted. Calls methods to achieve binary search, scramble, and linear search
    for (int i = 0; i < totalInput; i++) {
      System.out.print(array0[i] + " ");
    }
    System.out.println();
    System.out.print("Enter a grade to search for: ");
    int key = intScanner.nextInt();
    binary(array0, totalInput, key);
    scramble(array0, totalInput);
    System.out.print("Enter a grade to search for: ");
    key = intScanner.nextInt();
    linear(array0, totalInput, key);
  }
}