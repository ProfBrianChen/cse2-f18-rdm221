/////////
//// CSE 002 CSE2Linear
//// Ryan Matthiessen
//// Last Edited 11/26/2018
//// Generate a random array, delete a user-inputted index, and remove user-inputted elements (numbers). Do this until the user choses to stop

// Imports Scanner for user input and Random for generating the array
import java.util.Scanner;
import java.util.Random;

public class RemoveElements {
  
// Method 'randomInput' generates a random array of 9 numbers using Math.random(). Returns the generated array to the main method
  public static int[] randomInput () {
    
    int[] randomArray = new int[10];
    for (int i = 0; i < randomArray.length; i++) {
      randomArray[i] = (int)(Math.random() * 8 + 1);
    }
    return randomArray;
  }

// Method 'delete' takes the index 'pos' provided by the user and deletes it. Returns a new array 'newArray'
  public static int[] delete (int[] num, int pos) {

// Checks for input outside the index bounds (0-9). If detected, returns the original array 'num'
    if (pos > 9 || pos < 0) {
      System.out.println("The index is not valid");
      return num;
    }
// Declares and initializes a new array 'newArray' with parameters of one index less than array 'num'. Copies values from 'num' to 'newArray'
    int[] newArray = new int[num.length - 1];
    for (int i = 0; i < pos; i++) {
      newArray[i] = num[i];
    }
    for (int i = pos + 1; i <= newArray.length; i++) {
      newArray[i - 1] = num[i];
    }
// Prints the index 'pos' that is removed. Returns 'newArray'
    System.out.println("Index " + pos + " element is removed");
    return newArray;
  }
  
// Method 'remove' takes the element 'target' provided by the user and deletes all instances of it. Returns the array 'list', 'newArray', or 'finalArray'
  public static int[] remove (int[] list, int target) {

// Checks for input outside the element bounds (0-9). If detected, returns the array 'list'
    if (target > 9 || target < 0) {
      System.out.println("Element " + target + " was not found");
      return list;
    }
// Declares and initializes a new array 'newArray' with parameters the same as array 'list'. Copies all the values not equal to input 'target'. If equal, returns value '0' in the index of newArray.
    int counter = 0;
    int[] newArray = new int[10];
    for (int i = 0; i < list.length; i++) {
      if (list[i] != target) {
        newArray[counter] = list[i];
        counter++;
      }
    }
// Declares and initializes a new array 'finalArray' with parameters (10 - (amt of zeros)). Copies all the values that are not zero.
    int[] finalArray = new int[newArray.length - (10 - counter)];
    for (int i = 0; i < newArray.length; i++) {
      if (newArray[i] != 0) {
        finalArray[i] = newArray[i];
      }
    }
// If all indecies are maintained (not array values matching 'target'), then return state value was not found and return 'finalArray'
    if (counter == list.length) {
      System.out.println("Element " + target + " was not found");
      return finalArray;
    }
// Return state value was found and return 'finalArray'
    System.out.println("Element " + target + " has been found");
    return finalArray;
  }
  
// Method 'main' receives user input regarding index and target values. Calls methods and asks for input to keep the program running
  public static void main (String [] arg) {
    
    Scanner scan = new Scanner(System.in);
    int num[] = new int[10];
    int newArray1[];
    int newArray2[];
    int index, target;
    String answer = "";
    do {
      System.out.print("Random input 10 ints [0-9]");
      num = randomInput();
      String out = "The original array is:";
      out += listArray(num);
      System.out.println(out);
      
      System.out.print("Enter the index ");
      index = scan.nextInt();
      newArray1 = delete(num,index);
      String out1 = "The output array is ";
      out1 += listArray(newArray1); //return a string of the form "{2, 3, -9}"  
      System.out.println(out1);
      
      System.out.print("Enter the target value ");
      target = scan.nextInt();
      newArray2 = remove(newArray1, target);
      String out2 = "The output array is ";
      out2 += listArray(newArray2); //return a string of the form "{2, 3, -9}"  
      System.out.println(out2);
      
      System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
      answer = scan.next();
    } while (answer.equals("Y") || answer.equals("y"));
  }
  
// Method 'listArray' prints the numbers of the array in proper array format
  public static String listArray(int num[]){
    String out = "{";
    for(int j = 0; j < num.length; j++){
      if(j > 0){
        out += ", ";
      }
      out += num[j];
    }
    out += "} ";
    return out;
  }
}