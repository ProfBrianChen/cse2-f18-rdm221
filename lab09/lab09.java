/////////
//// CSE 002 lab09
//// Ryan Matthiessen
//// Last Edited 11/26/2018
//// Manipulates several arrays using inverter and copy methods

public class lab09 {
// Method 'copy' creates a new array 'copyArray' with the same values as the passed array and returns the new array 
  public static int[] copy (int[] array0) {
    int[] copyArray = new int[array0.length];
    for (int i = 0; i < array0.length; i++) {
      copyArray[i] = array0[i];
    }
    return copyArray;
  }
// Method 'inverter' takes the passed array 'invertArray' and copies it backward into itself so the first index is now the last, the second index is now the second to last, etc.
  public static void inverter (int[] invertArray) {
    int temp = 0;
    int arrayLength = invertArray.length;
    for (int i = 0; i < (arrayLength / 2); i++) {
      temp = invertArray[i];
      invertArray[i] = invertArray[arrayLength - 1 - i];
      invertArray[arrayLength - 1 - i] = temp;
    }
  }
// Method 'inverter2' copies the passed array 'array1' and put its into 'copyArray'. The array is then inverted using the method 'inverter' and the new, inverted array is promptly returned
  public static int[] inverter2 (int[] array1) {
    int[] copyArray = copy(array1);
    inverter(copyArray);
    return copyArray;
  }
// Method 'print' prints out the passed array
  public static void print (int[] inputArray) {
    for (int i = 0; i < inputArray.length; i++) {
      System.out.print(inputArray[i] + " ");
    }
    System.out.println();
  }
// Method 'main' declares and initializes the three arrays being used and calls the respective methods required for each array
  public static void main (String args[]) {
    int[] array0 = new int[] {1, 2, 3, 4, 5, 6, 7, 8};
    int[] array1 = copy(array0);
    int[] array2 = copy(array0);
    
    inverter(array0);
    inverter2(array1);
    int[] array3 = inverter2(array2);
    
    print(array0);
    print(array1);
    print(array3);
  }
}