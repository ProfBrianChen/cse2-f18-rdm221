/////////
//// CSE 002 Lab07
//// Ryan Matthiessen
//// Last Edited 10/25/2018
//// Create random sentences with the inclusion of adjectives, non-primary nouns, and past-tense verbs

import java.util.Scanner;
import java.util.Random;

public class Lab07 {
  
  public static String adjective () {
    Random randomGenerator = new Random();
    int randomInt = randomGenerator.nextInt(10);
    String adjective = "";
    switch (randomInt) {
      case 0:
        adjective = "diligent";
        break;
      case 1:
        adjective = "jolly";
        break;
      case 2:
        adjective = "fuzzy";
        break;
      case 3:
        adjective = "loving";
        break;
      case 4:
        adjective = "cowardly";
        break;
      case 5:
        adjective = "sincere";
        break;
      case 6:
        adjective = "attractive";
        break;
      case 7:
        adjective = "forgetful";
        break;
      case 8:
        adjective = "slimy";
        break;
      case 9:
        adjective = "crooked";
        break;
    }
    return adjective;
  }
  
  public static String subject () {
    Random randomGenerator = new Random();
    int randomInt = randomGenerator.nextInt(10);
    String subject = "";
    switch (randomInt) {
      case 0:
        subject = "candy cigarette dealer";
        break;
      case 1:
        subject = "director of operations";
        break;
      case 2:
        subject = "knight";
        break;
      case 3:
        subject = "insect";
        break;
      case 4:
        subject = "family";
        break;
      case 5:
        subject = "historian";
        break;
      case 6:
        subject = "college student";
        break;
      case 7:
        subject = "construction worker";
        break;
      case 8:
        subject = "professional basketball player";
        break;
      case 9:
        subject = "health inspector";
        break;
    }
    return subject;
    
  }
  
  public static String verb () {
    Random randomGenerator = new Random();
    int randomInt = randomGenerator.nextInt(10);
    String verb = "";
    switch (randomInt) {
      case 0:
        verb = "retired";
        break;
      case 1:
        verb = "applauded";
        break;
      case 2:
        verb = "begged";
        break;
      case 3:
        verb = "questioned";
        break;
      case 4:
        verb = "cheated";
        break;
      case 5:
        verb = "roasted";
        break;
      case 6:
        verb = "punished";
        break;
      case 7:
        verb = "avoided";
        break;
      case 8:
        verb = "educated";
        break;
      case 9:
        verb = "scolded";
        break;
    }
    return verb;
  }
  
  public static String object () {
    Random randomGenerator = new Random();
    int randomInt = randomGenerator.nextInt(10);
    String object = "";
    switch (randomInt) {
      case 0:
        object = "fireman";
        break;
      case 1:
        object = "aunt";
        break;
      case 2:
        object = "artist";
        break;
      case 3:
        object = "rock star";
        break;
      case 4:
        object = "snail";
        break;
      case 5:
        object = "train conductor";
        break;
      case 6:
        object = "cameraman";
        break;
      case 7:
        object = "pilot";
        break;
      case 8:
        object = "beach bum";
        break;
      case 9:
        object = "cyclops";
        break;
    }
    return object;
  }
  
  
  public static String thesis () {
    
    String subject = subject();
    System.out.println("The " + adjective() + " " + subject + " " + verb() + " the " + adjective() + " " + object() + ".");
    return subject;
  }
  
  public static void action (String subject) {
    Random randomGenerator = new Random();
    int amount = randomGenerator.nextInt(6);
    for (int i = 0; i < amount; i++) {
      int choice = randomGenerator.nextInt(2);
      switch (choice) {
        case 0:
          System.out.println("He " + "then " + verb() + " his " + adjective() + " " + object() + ".");
          break;
        case 1:
          System.out.println("The " + subject + " then " + verb() + " his " + adjective() + " " + object() + ".");
      }
    }
  }
  
  public static void conclusion () {
    System.out.println("Finally, he " + verb() + ", " + verb() + ", and " + verb() + " both the " + object() + " and the " + object() + ".");
  }
  
  public static void main (String args[]) {
    Scanner myScanner = new Scanner(System.in);
    char answer = 'N';
    String sentenceSubject = thesis();
    
    do {
      System.out.print("Would you like another sentence? (Y/N): " );
      answer = myScanner.nextLine().charAt(0);
      if (answer == 'Y') {
        sentenceSubject = thesis();
      } else if (answer == 'N') {
        action(sentenceSubject);
        conclusion();
      } else {
          System.out.print("That input did not register, please retype. ");
        }
    } while (answer != 'N');
  }
}