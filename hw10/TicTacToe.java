/////////
//// CSE 002 TicTacToe
//// Ryan Matthiessen
//// Last Edited 12/3/2018
//// Plays a game of tic-tac-toe, creating the board, updating it with each move, and having two players switch on and off. Checks for win after each move

// Imports Scanner for user input
import java.util.Scanner;

public class TicTacToe {
  
// Method 'updateBoard' finds the respective index for the overall index inputted. Places the player's character in that spot. Does not print out the board
  public static void updateBoard (String[][]board, int selection, String turn) {
    
    int row = (selection - 1) / 3;
    int column = (selection -1) % 3;
    board[row][column] = turn;
  }
  
// Method 'updateTurn' alternates between each player, allowing the other person to play the next move
  public static String updateTurn (String turn) {
    
    if (turn == "O") {
      turn = "X";
    } else if (turn == "X") {
      turn = "O";
    }
    return turn;
  }
  
// Method 'listArray' prints the board of tic-tac-toe
  public static void listArray (String[][] board) {
    for (int i = 0; i < 3; i++) {
      for (int j = 0; j < 3; j++) {
        System.out.print("  " + board[i][j] + "  ");
      }
      System.out.println();
    }
    System.out.println();
  }
  
// Method 'win' checks the horizontal, vertical, and diagonal dimensions for three in a row. Checks in just the row, column, and diagonal last placed. Returns winner if three in a row
  public static char win (String[][] board, char winner, int selection) {
// 'row' and 'column' are the indecies for the array based on the selection
    int row = (selection - 1) / 3;
    int column = (selection -1) % 3;
// 'OCounter' and 'XCounter' increment when horizontal, vertical, or diagonal values are the same
    int OCounter = 0;
    int XCounter = 0;
// Checks the horizontal condition, keeping the row consistent
    for (int i = 0; i < 3; i++) {
      if (board[row][i] == "O") {
        OCounter++;
      } else if (board[row][i] == "X") {
        XCounter++;
      }
    }
    if (OCounter == 3 || XCounter == 3) {
      winner = 'Y';
      return winner;
    }
// Checks the vertical condition, keeping the column consistent
    OCounter = 0;
    XCounter = 0;
    for (int i = 0; i < 3; i++) {
      if (board[i][column] == "O") {
        OCounter++;
      } else if (board[i][column] == "X") {
        XCounter++;
      }
    }
    if (OCounter == 3 || XCounter == 3) {
      winner = 'Y';
      return winner;
    }
// Checks one diagonal condition
    OCounter = 0;
    XCounter = 0;
    for (int i = 0; i < 3; i++) {
      if (board[i][i] == "O") {
        OCounter++;
      } else if (board[i][i]  == "X") {
        XCounter++;
      }
    }
    if (OCounter == 3 || XCounter == 3) {
      winner = 'Y';
      return winner;
    }
// Checks the other diagonal condition
    OCounter = 0;
    XCounter = 0;
    for(int i = 0, j = 2; i < 3; i++,j--){
      if (board[i][j]  == "O") {
        OCounter++;
      } else if (board[i][j] == "X") {
        XCounter++;
      }
    }  
    if (OCounter == 3 || XCounter == 3) {
      winner = 'Y';
    }
    return winner;
  }
  
  public static void main (String args[]) {
// Declares and initializes many variables which will be used throughout the program
    Scanner intScanner = new Scanner(System.in);
    int selection;
    int counter = 0;
    char winner = 'N';
    String turn = "O";
    String[][] board = {
      {"1","2","3"},
      {"4","5","6"},
      {"7","8","9"},
    };
// Prints 'board' and requests user to enter input
    while (winner == 'N' && counter < 9) {
      listArray(board);
      System.out.print("Player " + turn + " please type the integer to replace for your move: ");
// Checks to make sure input is of type int
      while (!intScanner.hasNextInt()) {
        System.out.print("Make sure your input is an integer: ");
        intScanner.next();
      }
      selection = intScanner.nextInt();
// Checks to make sure input is between 1-9 (and is an int)
      while (selection > 9 || selection < 1) {
        System.out.print("Make sure your input is an integer is between 1-9: ");
        if (intScanner.hasNextInt()) {
          selection = intScanner.nextInt();
        }
      }
// Checks to make sure input has not been entered already
      while (board[(selection - 1) / 3][(selection - 1) % 3] == "X" || board[(selection - 1) / 3][(selection - 1) % 3] == "O") {
        System.out.print("Make sure no-one has already played this spot: ");
        if (intScanner.hasNextInt()) {
          selection = intScanner.nextInt();
        }
      }
// Updates the board, checks for a winner. If winner, prints who it is. If no winner, continues playing and updates the turn. If 9 rounds have been played, the game is a draw.
      updateBoard(board, selection, turn);
      winner = win(board, winner, selection);
      counter++;
      if (winner == 'Y') {
        System.out.println("Player " + turn + " has won this game! Thanks for playing!");
        listArray(board);
        break;
      }
      if (winner == 'N' && counter == 9) {
        System.out.println("The game is a draw! Thanks for playing!");
        listArray(board);
        break;
      }
      turn = updateTurn(turn);
    }
  }
}