/////////
//// CSE 002 lab08
//// Ryan Matthiessen
//// Last Edited 11/10/2018
//// Initialize one array (0-99) with a random set of numbers and compare them to the index of the other array.

  // Imports scanner class to allow for user input
import java.util.Scanner;

public class lab08 {
  
  public static void main (String args[]) {
  
  // Declares an array 'randomArray' with 100 elements and initializes each element with a random number (0 - 99)
    int[] randomArray = new int[100];
    for (int index = 0; index < 100; index++) {
      randomArray[index] = (int)(Math.random() * 100);
    }
    
  // Prints the number in each element of 'randomArray'
    System.out.print("My random array has numbers: ");
    for (int index = 0; index < 100; index++) {
      System.out.print(randomArray[index] + " ");
    }
    System.out.println();
    
  /* Declares an array 'matchArray' with 100 elements and initializes them with their corresponding index (0 - 99)
     Looks for matching numbers betweeen the elements in 'randomArray' and increments them as 'match' variable temporarily.
     Match variable will then be transferred to the index in 'matchArray' of the number being counted */
    int[] matchArray = new int[100];
    int count = 0;
    for (count = 0; count < 100; count++) {
      int match = 0;
      for (int index = 0; index < 100; index++) {
        if (count == randomArray[index]) {
          match++;
        }
      }
      matchArray[count] = match;
   // If statement to determine proper diction when printing out values
      if (match == 1) {
        System.out.println(count + " occurs " + matchArray[count] + " time.");
      } else if (match >= 1) {
        System.out.println(count + " occurs " + matchArray[count] + " times.");
      }
    }
  }
}