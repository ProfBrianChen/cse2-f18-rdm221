/////////////
//// CSE 002 Arithmetic
//// Ryan Matthiessen
//// Last edited: 9/8/2018
//// Compute the cost of the items bought, including PA sales tax of 6%

public class Arithmetic{
  
  public static void main(String args[]){
    
    // Variable declaration and assignment
    
    int numPants = 3; // Number of pants purchased
    double pantsPrice = 34.98; // Cost per pant
    
    int numSweatshirts = 2; // Number of sweatshirts purchased
    double sweatshirtsPrice = 24.99; // Cost per sweatshirt
    
    int numBelts = 1; // Number of belts purchased
    double beltPrice = 33.99; // Cost per belt
    
    double paSalesTax = 0.06; // The tax rate
    
    // Calculates the total cost of each item type, sales tax of each item type, total cost of order before and after tax, and total sales tax
    
    double totalCostOfPants = numPants * pantsPrice;
    double salesTaxPants = (totalCostOfPants * paSalesTax * 100);
    salesTaxPants = (int)salesTaxPants / 100.0;
    
    double totalCostOfSweatshirts = numSweatshirts * sweatshirtsPrice;
    double salesTaxSweatshirts = (totalCostOfSweatshirts * paSalesTax * 100);
    salesTaxSweatshirts = (int)salesTaxSweatshirts / 100.0;
    
    double totalCostOfBelts = numBelts * beltPrice;
    double salesTaxBelts = (totalCostOfBelts * paSalesTax * 100);
    salesTaxBelts = (int)salesTaxBelts / 100.0;
    
    double totalCostNoTax = ((totalCostOfPants + totalCostOfSweatshirts + totalCostOfBelts) * 100);
    totalCostNoTax = (int)totalCostNoTax / 100.0;
    
    double salesTaxTotal = ((salesTaxPants + salesTaxSweatshirts + salesTaxBelts) * 100);
    salesTaxTotal = (int)salesTaxTotal / 100.0;
    
    double totalCostWithTax = ((totalCostNoTax + salesTaxTotal) * 100);
    totalCostWithTax = (int)totalCostWithTax / 100.0;
    
    // Prints the total cost of each item, as well as the total cost of the entire order with and without sales tax
    System.out.println("The total cost of the pants are $" + totalCostOfPants + " plus a sales tax of $" + salesTaxPants + ".");
    System.out.println("The total cost of the sweatshirts are $" + totalCostOfSweatshirts + " plus a sales tax of $" + salesTaxSweatshirts + ".");
    System.out.println("The total cost of the belts are $" + totalCostOfBelts + " plus a sales tax of $" + salesTaxBelts + ".");
    System.out.println("The total cost of the order is $" + totalCostNoTax + " without sales tax.");
    System.out.println("The total sales tax of the order is $" + salesTaxTotal + ".");
    System.out.println("The total cost of the order is $" + totalCostWithTax + " with sales tax.");
    
  }
}