/////////
//// CSE 002 EncryptedX
//// Ryan Matthiessen
//// Last Edited 10/20/2018
//// Ask the user to input a side length for a square, encrypt an 'x' inside the square with the absence of stars.

  // Imports the scanner for user input
import java.util.Scanner;

public class EncryptedX {
  
  // Creation of method 'encrypt' to print the square and encrypted X. Not necessary, but wanted experience with methods
  public static void encrypt (int sideLength, char star) {
    
  // For loop for incrementing the vertical dimension of the square
    for (int i = 1; i <= sideLength; i++) {
  
  // For loop for setting the blank spaces inside the square (horizontal dimension)
      for (int j = 1; j <= sideLength; j++) {
        if (i == j) {
          System.out.print(" ");  
        } else if (i + j == (sideLength + 1)) {
          System.out.print (" ");
        } else {
          System.out.print(star + "");
        }
      }
      System.out.println("");
    }
  }
    
  public static void main (String args[]) {

    // Declare and initialize variables used to determine dimensions and charracteristics of the square
    Scanner intScanner = new Scanner(System.in);
    char star = '*';
    
    // Ask the user for integer input and checks if the user inputted an integer
    System.out.print("Please enter the side length of the square (0-100) for encryption: ");
    while (!intScanner.hasNextInt()) {
      System.out.print("Make sure your input is an integer. Please re-enter: ");
      intScanner.next();
    }
    
    // Stores the integer given in 'sideLength'
    int sideLength = intScanner.nextInt();

    // If the integer input is not between 0-100, have the user re-input and store it back in 'sideLength'    
    if (sideLength > 100 || sideLength < 0) {
      while (sideLength > 100 || sideLength < 0) {
        System.out.print("Make sure your integer is no less than 0 and no greater than 100. Please re-enter: ");
        sideLength = intScanner.nextInt();
      }
    }
    
    // Calls the method 'encrypt' that was written above
    encrypt(sideLength, star);
  }
}